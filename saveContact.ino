// C++ code

#define maxStringLength 50 // Maximum length of the string


// Include necessary libraries
#include <Adafruit_LiquidCrystal.h>
#include <Keypad.h>
#include <EEPROM.h>


// Define default state
String state = "dialing";

//Cursor starting x coord, used to move cursor when typing
int x_coord = 0;

// Buzzer pin.
int buzzerPin = 10;

// String to store dialed number.
String dialedNumber = "";

// String to store contact name.
String contactName = "";

// Initialize the liquidcrystal lib.
Adafruit_LiquidCrystal lcd(0);

// Constants for the number of rows and cols of the keypad.
const char number_of_rows = 4;
const char number_of_columns = 4;

// Define row pins and col pins.
byte row_pins[number_of_rows] = {9, 8, 7, 6};
byte column_pins[number_of_columns] = {5, 4, 3, 2};

//Define the array representing the keypad layout.
char key_array[number_of_rows][number_of_columns] = {  
  {'1', '2', '3', 'A'},
  {'4', '5', '6', 'B'},
  {'7', '8', '9', 'C'},
  {'*', '0', '#', 'D'}
};

// Make a Keypad object.
Keypad k = Keypad(makeKeymap(key_array),row_pins , column_pins, number_of_rows, number_of_columns);


String readStringFromEEPROM(int addr) {
  char charBuf[maxStringLength]; // Create a buffer to store the characters read from EEPROM
  int i = 0;
  char currentChar = EEPROM.read(addr); // Read the first character from EEPROM

  // Read characters until null terminator is encountered or maximum string length is reached
  while (currentChar != '\0' && i < maxStringLength - 1) {
    charBuf[i] = currentChar; // Store the current character in the buffer
    i++;
    currentChar = EEPROM.read(addr + i); // Read the next character from EEPROM
  }
  charBuf[i] = '\0'; // Null-terminate the buffer to form a valid C-style string
  
  // Convert the character buffer to a String
  String str = String(charBuf);
  
  // Extract the first three characters
  String number = str.substring(0, 3);
  
  // Extract the rest of the characters
  String name = str.substring(3);
  // Display the formatted output
  
  lcd.clear();
  lcd.print("Number: " + number);
  lcd.setCursor(0, 1);
  lcd.print("Name: " + name);
  Serial.print("Number: ");
  Serial.println(number);
  Serial.print("Name: ");
  Serial.println(name);
  
  delay(350);
  lcd.clear();
  return str; // Return the entire string
}


void setup()
{
  // Initialize Serial communication.
  Serial.begin(9600);
  
  // Initialize lcd screen.
  lcd.begin(16, 2);
  
  // Set up buzzerPin on Output mode.
  pinMode(buzzerPin, OUTPUT);
  
  // Display menu key infos.
  lcd.print("'A': OK '*': Del");
  lcd.setCursor(0, 1);
  lcd.print("'#': Home");
  delay(1000);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Enter Number:");
  
  // Set cursor at current x coords.
  lcd.setCursor(0, 1);
}



void loop() {
  // Display a blinking cursor
  lcd.blink();
  delay(350);
  
  // Get key pressed from the keypad
  char key_pressed = k.getKey();
  if (key_pressed) {
    digitalWrite(buzzerPin, HIGH);  
    
    // Check if the key pressed is a digit
    if (state == "dialing"){
      if(isdigit(key_pressed)) {
        // Append the digit to the dialed number
        dialedNumber += key_pressed;
        lcd.print(key_pressed);
        Serial.println(dialedNumber);
        x_coord += 1;
      }

      if (key_pressed == '*') {
        if (dialedNumber.length() > 0) {
          // Remove the last character from dialedNumber.
          dialedNumber.remove(dialedNumber.length() - 1);
          // Move cursor to the last position and overwrite the character with a space.
          lcd.setCursor(dialedNumber.length(), 1);
          // Overwrite character with space.
          lcd.write(' ');
          // Move the cursor back 1 bit.
          x_coord -= 1;
          // Move cursor back to the deleted character position.
          lcd.setCursor(x_coord, 1);
        }  
      }
      if (key_pressed == 'A') {
        if (dialedNumber.length() == 3) { //10
          for (int i = 0; i < dialedNumber.length(); i++) {
            EEPROM.write(i, dialedNumber[i]);
          }
          // Null-terminate the string
          EEPROM.write(dialedNumber.length(), '\0');
          lcd.clear();
          state = "contact name";
          x_coord = 0;
          lcd.print("Enter Name:");
        }
      } // End of validation if statement.
    } // End of dialing state if statement.
    if (state == "contact name") {
      Serial.println("state changed!");
      if(isAlpha(key_pressed)) {
        // Set cursor at current x coords.
        lcd.setCursor(x_coord, 1);
        // Append the letter to the contactName.
        contactName += key_pressed;
        lcd.print(key_pressed);
        Serial.println(contactName);
        x_coord += 1;
      } // End of typing if statement
      
      if (key_pressed == '*') {
        if (contactName.length() > 0) {
          // Remove the last character from contactName.
          contactName.remove(contactName.length() - 1);
          // Move cursor to the last position and overwrite the character with a space.
          lcd.setCursor(contactName.length(), 1);
          // Overwrite character with space.
          lcd.write(' ');
          // Move the cursor back 1 bit.
          x_coord -= 1;
          // Move cursor back to the deleted character position.
          lcd.setCursor(x_coord, 1);
        }  
      } // End of suppression if statement.
      
      if (key_pressed == '#') {
        lcd.clear(); // Clear the LCD screen.

        for (int i = 0; i < contactName.length(); i++) {
          EEPROM.write(i+dialedNumber.length(), contactName[i]);
        }
        
        String storedData = readStringFromEEPROM(0); // Read string from EEPROM starting at address 0
        lcd.print("Contact saved!");
        dialedNumber = "";
        contactName = "";
        state == "dialing";
      }
      
    } // End of "contact Name" state else statement.
  } // End of key_pressed if statement
  else {
  	digitalWrite(buzzerPin, LOW);
  }
}


