#include <Keypad.h>

#include <Adafruit_LiquidCrystal.h>



#define MAIN_MENU 0
#define SECONDARY_MENU 1

int state = MAIN_MENU;

const int ROW_NUM    = 4; // Quatre lignes
const int COLUMN_NUM = 4; // Quatre colonnes

Adafruit_LiquidCrystal lcd(0);

char keys[ROW_NUM][COLUMN_NUM] = {
  {'1','2','3', 'A'},
  {'4','5','6', 'B'},
  {'7','8','9', 'C'},
  {'*','0','#', 'D'}
};

byte pin_rows[ROW_NUM] = {9,8,7,6};      // Connectez aux broches de ligne du clavier
byte pin_column[COLUMN_NUM] = {5,4,3,2}; // Connectez aux broches de colonne du clavier

Keypad k = Keypad(makeKeymap(keys), pin_rows, pin_column, ROW_NUM, COLUMN_NUM);

const int buzzerPin = 10; // Connectez votre buzzer passif à la broche 10

String menuOptions[] = {"1.Play", "2.Create"};
int numMenuOptions = 2; // Nombre total d'options de menu

String musicMenuOptions[] = {"1.Melody 1", "2.Melody 2","3.Melody 3","4.Melody 4","5.Melody 5"};
int numMusicMenuOptions = 5;

int melodySize = 16;

int melody1[] = {262, 294, 330, 349, 392, 440, 494, 523, 587, 659, 698, 784, 880, 988, 1047, 1175};
int melody2[] = {294, 330, 349, 392, 440, 494, 523, 587, 659, 698, 784, 880, 988, 1047, 1175, 1319};
int melody3[] = {330, 349, 392, 440, 494, 523, 587, 659, 698, 784, 880, 988, 1047, 1175, 1319, 1397};
int melody4[] = {349, 392, 440, 494, 523, 587, 659, 698, 784, 880, 988, 1047, 1175, 1319, 1397, 1568};
int melody5[] = {392, 440, 494, 523, 587, 659, 698, 784, 880, 988, 1047, 1175, 1319, 1397, 1568, 1760};

struct Melody {
  int* notes;
  int length;
};

Melody melodies[] = {
  {melody1, melodySize},
  {melody2, melodySize},
  {melody3, melodySize},
  {melody4, melodySize},
  {melody5, melodySize}
};


bool scrolling = true; // Variable pour contrôler le défilement

int selectedMelody = -1;

void musicPlayMenu(char key_pressed) {
  if (scrolling) {
    lcd.clear();
    lcd.print("Select a number:");
    lcd.setCursor(0, 1);
    lcd.print("#: Music Menu");

    for (int optionIndex = 0;how manage brightness of lcd ic2 and keypad arduino ? optionIndex < numMusicMenuOptions; optionIndex++) {
      delay(100);
      lcd.clear();
      lcd.setCursor(0, 1);
      lcd.print(musicMenuOptions[optionIndex]);
      delay(100);

      if (optionIndex == numMusicMenuOptions - 1) {
        scrolling = false;
        state = SECONDARY_MENU;
      }
    }
  }

  if (key_pressed >= '1' && key_pressed <= '5') {
    selectedMelody = key_pressed - '1';
    lcd.clear();
    lcd.print("Song selected:");
    lcd.setCursor(0, 1);
    lcd.print(key_pressed);
    lcd.print(" Melody ");
    lcd.print(selectedMelody + 1);
    playMelody(selectedMelody);
  } else {
    lcd.setCursor(0, 1);
    lcd.print("Invalid");
  }
  while (key_pressed); 
  delay(100);
}

void setup() {
  lcd.begin(16, 2);
  Serial.begin(9600);
  lcd.print("Music menu");
  lcd.setBacklight(1);
  pinMode(buzzerPin, OUTPUT);
}

void loop() {
  switch (state) {
    case MAIN_MENU:
      if (scrolling) {
        lcd.clear();
        lcd.print("Select a number:");
        lcd.setCursor(0, 1);
        lcd.print("#: Menu");

        for (int optionIndex = 0; optionIndex < numMenuOptions; optionIndex++) {
          delay(100);
          lcd.clear();
          lcd.setCursor(0, 1);
          lcd.print(menuOptions[optionIndex]);
          delay(100);

          if (optionIndex == numMenuOptions - 1) {
            scrolling = false;
            state = SECONDARY_MENU;
          }
        }
      }
      break;

    case SECONDARY_MENU:
      char key_pressed = k.getKey();
      if (key_pressed) {
        lcd.clear();
        lcd.print("Option selected: ");
        switch (key_pressed) {
          case '1':
            lcd.setCursor(0, 1);
          	delay(100);
            lcd.print("Play");
          	scrolling = true;
            musicPlayMenu(key_pressed); 
            break;
          case '2':
            lcd.setCursor(0, 1);
            lcd.print("Create");
            break;
          case '#':
            lcd.setCursor(0, 1);
            lcd.print("Back to menu");
            state = MAIN_MENU;
            break;
          default:
            lcd.setCursor(0, 1);
            lcd.print("Invalid");
            break;
        }
        while (k.getKey()); 
        delay(100);
      }
      break;
  }
}

void playMelody(int melodyIndex) {
  Melody melody = melodies[melodyIndex];
  int noteDuration = 500;
  for (int i = 0; i < melody.length; i++) {
    int noteFrequency = melody.notes[i];
    if (noteFrequency == 0) break;
    tone(buzzerPin, noteFrequency);
    delay(noteDuration);
    noTone(buzzerPin);
    delay(50);
  }
}

