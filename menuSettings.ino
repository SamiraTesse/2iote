#include <Keypad.h>
#include <LiquidCrystal.h>

#define main_menu 0
#define secondary 1
#define brightness_setting 2
#define volume_setting 3


int state = main_menu;

const int ROW_NUM    = 4;
const int COLUMN_NUM = 4;

LiquidCrystal lcd(12, 11, 5, 4, 3, 2); // Broches RS, E, D4, D5, D6, D7

char keys[ROW_NUM][COLUMN_NUM] = {
  {'1','2','3', 'A'},
  {'4','5','6', 'B'},
  {'7','8','9', 'C'},
  {'*','0','#', 'D'}
};

byte pin_rows[ROW_NUM] = {9,8,7,6};
byte pin_column[COLUMN_NUM] = {5,4,3,2};

Keypad k = Keypad(makeKeymap(keys), pin_rows, pin_column, ROW_NUM, COLUMN_NUM);

// Tableau des options de menu
String settingMenuOptions[] = {"1.Volume", "2.Brightness"};
int numSettingMenuOptions = 2; // Nombre total d'options de menu
bool scrolling = true; // Variable pour contrôler le défilement

void setup() {
  lcd.begin(16, 2); // Initialisez l'écran LCD avec 16 colonnes et 2 lignes
  Serial.begin(9600);
  lcd.print("Settings Menu");
}

void loop() {
  
  switch(state){
    
    case main_menu:
        if (scrolling) {

          // Affichez le message pour saisir le numéro de l'option
          lcd.clear();
          lcd.print("Saisissez le numero:");
          lcd.setCursor(0,1);
          lcd.print("#: Menu");


          // Faites défiler les options de menu en dessous du texte "Menu Supfone:"
          for (int optionIndex = 0; optionIndex < numSettingMenuOptions; optionIndex++) {
            delay(100); // Délai pour afficher chaque option
            lcd.clear();
            lcd.setCursor(0, 1);
            lcd.print(settingMenuOptions[optionIndex]);
            delay(100); // Délai pour lire chaque option

            // Arrêtez le défilement si nous avons atteint la dernière option
            if (optionIndex == numSettingMenuOptions - 1) {
              scrolling = false;
              state = secondary;

            }
          }
       }
      break;
    
    case secondary:

        // Attendez que l'utilisateur saisisse une touche
          char key_pressed = k.getKey();
          if (key_pressed) {
            lcd.clear();
            lcd.print("Option selected: ");
            switch(key_pressed) {
              case '1':
                lcd.setCursor(0, 1);
                lcd.print("Volume");
                break;
              case '2':
                lcd.setCursor(0, 1);
                lcd.print("brightness");
              	scrolling = true;
              	state = brightness_setting;
                break;
              case '#':
                lcd.setCursor(0,1);
                lcd.print("Back to Menu");
                scrolling = true;
                state= main_menu;
              
                break;

              default:
                lcd.setCursor(0, 1);
                lcd.print("Invalid");
                break;
              
      
       case brightness_setting:
              lcd.clear();
              lcd.print("Manage Brightness");
              lcd.setCursor(0, 1);
              lcd.print("with potentiometer");
              break;
              
              
       case volume_setting:
              lcd.clear();
              lcd.print("Manage volume");
              lcd.setCursor(0, 1);
              lcd.print("with potentiometer");
              break;
            }
            while (k.getKey()); // Attendre que l'utilisateur relâche la touche
            delay(1000); // Ajouter un délai pour éviter les doubles saisies
          }  

        }
  
}
