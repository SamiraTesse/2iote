// C++ code

#define maxStringLength 50 // Maximum length of the string
#define maxPhoneNumberLength 3
#define MENU 0
#define CONTACT_LIST 1


// Include necessary libraries
#include <Adafruit_LiquidCrystal.h>
#include <Keypad.h>
#include <EEPROM.h>


// Define default state
int state = MENU;

// Buzzer pin.
int buzzerPin = 10;

// Initialize the liquidcrystal lib.
Adafruit_LiquidCrystal lcd(0);

// Constants for the number of rows and cols of the keypad.
const char number_of_rows = 4;
const char number_of_columns = 4;

// Define row pins and col pins.
byte row_pins[number_of_rows] = {9, 8, 7, 6};
byte column_pins[number_of_columns] = {5, 4, 3, 2};

//Define the array representing the keypad layout.
char key_array[number_of_rows][number_of_columns] = {  
  {'1', '2', '3', 'A'},
  {'4', '5', '6', 'B'},
  {'7', '8', '9', 'C'},
  {'*', '0', '#', 'D'}
};

// Make a Keypad object.
Keypad k = Keypad(makeKeymap(key_array),row_pins , column_pins, number_of_rows, number_of_columns);


String readStringFromEEPROM(int addr) {
  char charBuf[maxStringLength]; // Create a buffer to store the characters read from EEPROM
  int i = 0;
  char currentChar = EEPROM.read(addr); // Read the first character from EEPROM

  // Read characters until null terminator is encountered or maximum string length is reached
  while (currentChar != '\0' && i < maxStringLength - 1) {
    charBuf[i] = currentChar; // Store the current character in the buffer
    i++;
    currentChar = EEPROM.read(addr + i); // Read the next character from EEPROM
  }
  charBuf[i] = '\0'; // Null-terminate the buffer to form a valid C-style string
  
  // Convert the character buffer to a String
  String str = String(charBuf);
  
  // Extract the first three characters
  String number = str.substring(0, 3);
  
  // Extract the rest of the characters
  String name = str.substring(3);
  // Display the formatted output
  
  lcd.clear();
  lcd.print("Number: " + number);
  lcd.setCursor(0, 1);
  lcd.print("Name: " + name);
  Serial.print("Number: ");
  Serial.println(number);
  Serial.print("Name: ");
  Serial.println(name);
  
  delay(350);

  return str; // Return the entire string
}


void clearEEPROM() {
  for (int i = 0; i < maxPhoneNumberLength + maxStringLength; i++) {
    EEPROM.write(i, 0); // Overwrite EEPROM data with null characters
  }
}  


void setup()
{
  // Initialize Serial communication.
  Serial.begin(9600);
  
  // Initialize lcd screen.
  lcd.begin(16, 2);
  
  // Set up buzzerPin on Output mode.
  pinMode(buzzerPin, OUTPUT);

}


void loop() {
  // Display a blinking cursor
  lcd.blink();
  delay(350);
  
  // Get key pressed from the keypad
  char key_pressed = k.getKey();
  switch (state){
    case MENU:
      // Display menu key infos.
      lcd.clear();
      lcd.print("1.See contacts");
      lcd.setCursor(0, 1);
      lcd.print("2.Del conctact");
      delay(1000);
      state = CONTACT_LIST;
      break;
    	
    case CONTACT_LIST:
      if (key_pressed) {
        //state = 
        digitalWrite(buzzerPin, HIGH); 

        switch (key_pressed) {
          case '1':
            // Add input to select a specific contact using user input to get an address.
            lcd.clear();
            lcd.print(readStringFromEEPROM(0));
            break;

          case '2':
            lcd.clear();
            clearEEPROM();
            lcd.print("Contact deleted");
            state = MENU;
            break;
        }
      } else {
      	digitalWrite(buzzerPin, LOW); 
      }
      break;

    }	


}



