#include <Keypad.h>
#include <Adafruit_LiquidCrystal.h>

const int ROW_NUM    = 4; // Quatre lignes
const int COLUMN_NUM = 4; // Quatre colonnes

Adafruit_LiquidCrystal lcd(0);

char keys[ROW_NUM][COLUMN_NUM] = {
  {'1','2','3', 'A'},
  {'4','5','6', 'B'},
  {'7','8','9', 'C'},
  {'*','0','#', 'D'}
};

byte pin_rows[ROW_NUM] = {9,8,7,6};      // Connectez aux broches de ligne du clavier
byte pin_column[COLUMN_NUM] = {5,4,3,2}; // Connectez aux broches de colonne du clavier

Keypad k = Keypad(makeKeymap(keys), pin_rows, pin_column, ROW_NUM, COLUMN_NUM);

// Tableau des options de menu
String menuOptions[] = {"1.Contacts", "2.Messages", "3.Musique", "4.Settings"};
int numMenuOptions = 4; // Nombre total d'options de menu
bool scrolling = true; // Variable pour contrôler le défilement

void setup() {
  lcd.begin(16, 2); // Initialisez l'écran LCD avec 16 colonnes et 2 lignes
  Serial.begin(9600);
  lcd.print("Menu Supfone");
  lcd.setBacklight(1);
}

void loop() {
  
  
  if (scrolling) {
    
    // Affichez le message pour saisir le numéro de l'option
  	lcd.clear();
  	lcd.print("Saisissez le numero:");
  
    
    // Faites défiler les options de menu en dessous du texte "Menu Supfone:"
    for (int optionIndex = 0; optionIndex < numMenuOptions; optionIndex++) {
    delay(100); // Délai pour afficher chaque option
    lcd.clear();
    lcd.setCursor(0, 1);
    lcd.print(menuOptions[optionIndex]);
    delay(100); // Délai pour lire chaque option
    
    // Arrêtez le défilement si nous avons atteint la dernière option
    if (optionIndex == numMenuOptions - 1) {
      scrolling = false;

    }
    }
 }
  
  
 
// Attendez que l'utilisateur saisisse une touche
  char key_pressed = k.getKey();
  if (key_pressed) {
    lcd.clear();
    lcd.print("Option selected: ");
    switch(key_pressed) {
      case '1':
      	lcd.setCursor(0, 1);
        lcd.print("Contacts");
        break;
      case '2':
      	lcd.setCursor(0, 1);
        lcd.print("Messages");
        break;
      case '3':
      	lcd.setCursor(0, 1);
        lcd.print("Musique");
        break;
      case '4':
      	lcd.setCursor(0, 1);
        lcd.print("Settings");
        break;
      default:
      	lcd.setCursor(0, 1);
        lcd.print("Invalide");
        break;
    }
    while (k.getKey()); // Attendre que l'utilisateur relâche la touche
    delay(1000); // Ajouter un délai pour éviter les doubles saisies
  }
}
